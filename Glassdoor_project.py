# %%
import string
import time
from time import sleep

import pandas as pd
import requests
from fuzzywuzzy import process, fuzz
from collections import Counter
from datetime import datetime

# Selenium Webdriver:
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common import exceptions 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import Select 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# %%
path_chromedriver = r'C:\Users\jonathan.kammering\OneDrive - Statista GmbH\Desktop\Python\Scraping\system\chromedriver_win32\chromedriver.exe'
options = Options()
options.page_load_strategy = 'eager'
# options.add_argument('--headless')
options.add_argument("--window-size=1920,1080")
driver = webdriver.Chrome(path_chromedriver, options=options)
# %%
f_mail = 's.meischke91@gmail.com'
f_pass = 'Varianz23'
driver.get('https://www.glassdoor.de/member/home/index.htm')
time.sleep(2)
driver.find_element_by_id('onetrust-accept-btn-handler').click()
time.sleep(2)
email_field = driver.find_element_by_id('inlineUserEmail')
email_field.send_keys(f_mail)
pass_field = driver.find_element_by_id('inlineUserPassword')
pass_field.send_keys(f_pass)
driver.find_element_by_css_selector('#InlineLoginModule > div > div.mx-auto.my-0.maxw-sm-authInlineInner.px-std.px-sm-xxl.d-flex.flex-column.mw-400 > div > form > div.d-flex.align-items-center.flex-column > button > span'
                                    ).click()
# %%
filep = r'C:\Users\jonathan.kammering\OneDrive - Statista GmbH\Desktop\Python\Scraping\Scraping_intel\DATA\Kopie von datadummy_glassdoor.xlsx'
#create DataFrame for use in the iteration
df = pd.read_excel(filep, sheet_name='unternehmen', usecols='A:B')

names = []
names_freq = Counter()
# ambiguous results are of concern here, as we do the Fuzzy Match on them
if len(df) > 100:
    for i, row in df.iterrows():
        name = row['Arbeitgeber']
        names.append(name)
    for name in names: 
        names_freq.update(str(name).split(' '))
    # the list of words "names_freq" is boiled down to the 19 most frequent words, for words less than five digits long
    abbreviations = [word.lower() for (word,_) in names_freq.most_common(19) if len(word) < 5 and not word == '']
    # ... and again for words longer than 5 digits
    abbreviations_long = [word.lower() for (word,_) in names_freq.most_common(19) if len(word) > 5 and not word == '']
    # the abbreviations list is extended for the longer words
    abbreviations.extend(abbreviations_long)
    print(abbreviations)
else:
    abbreviations = []
# %%
def cleaning(company):
    clean = ''
    char = company.lower().split()
    first_char = char.pop(0)
    for element in char:
        if element not in abbreviations:
            clean = clean + ' ' + element
        else:
            continue
    clean = first_char + clean
    return clean
# %%
def matching(alts, title):
    names = []
    comprehensive =[]
    choice = ''
    pick = ''
    # using the FuzzyWuzzy package, two matches are performed: partial and sorted ratio, they both claculate the distance in edits from one string to another using tokenization
    for i in range(0, len(alts)):
        # the partial ratio checks whether the exact string is found anywehre in the string
        partial_ratio = fuzz.partial_ratio(cleaning(alts[i][0].lower()), cleaning(title.lower()))
        # sorts the tokens in each string and then calculates the distance, helpful when words are mixed up
        sorted_ratio = fuzz.partial_token_sort_ratio(cleaning(alts[i][0].lower()), cleaning(title.lower()))
        # if either is over our threshold, it is added to both the names and the comprehensive list
        if partial_ratio > 75 or sorted_ratio > 75:
            # we use two different lists here, because in the next matching step we need a list of only names, while the result of sorted_ratio and partial_ratio feed tuples as so: (Company, 89) into the list
            # thus we save only the names in "names" and the tuples in "comprehensive"
            names.append(alts[i][0])
            comprehensive.append(alts[i])
        else:
            pass
    # this process extracts the name with the best match to "title" from the "names" list
    pick = process.extractOne(title, names)
    # to be able to return the tuple of our pick, we match the first part of each tuple (the name), with the names lists content
    if len(names) > 0:
        for i in range(0, len(comprehensive)):
            if comprehensive[i][0] == pick[0]:
                choice = comprehensive[i]
                break
            else:
                pass
    else:
        choice = None
    return choice
# %%
# Scrape profile data
driver.maximize_window()
# define resulting DataFrame
company_data = pd.DataFrame(columns=('Input_name',
                                        'glassdoor_name',
                                        'city',
                                        'friend_recc',
                                        'ceo_recc',
                                        'interviewexperience_positive',
                                        'interviewexperience_negative',
                                        'interviewexperience_neutral',
                                        'int_type_online-bewerbung',
                                        'int_type_personalvermittler',
                                        'int_type_empfehlung',
                                        'int_type_hochschul-recruiting',
                                        'int_type_persönlich',
                                        'int_type_sonstiges',
                                        'int_type_vermittlungsagentur',
                                        'link/request',
                                        'status'))
no_results_df = pd.DataFrame(columns=('name', 'city', 'status'))

number = 0

for i, row in df[:1].iterrows():#len(df)):
    int_positive = None
    int_neutral = None
    int_negative = None
    detail_parameter_a = None
    detail_parameter_b = None
    detail_parameter_c = None
    detail_parameter_d = None
    detail_parameter_e = None
    detail_parameter_f = None
    detail_parameter_g = None
    choice = None
    ceo_percent = None
    recc_percent = None
    profile_details = []
    number += 1
    # reading information from specific row of "df"
    name = row['Arbeitgeber']
    city = row['Hauptsitz Stadt']
    print('request #'+str(number)+' ('+name+')')
    driver.get('https://www.glassdoor.de/member/home/index.htm')
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
                                    (By.CSS_SELECTOR,
                                    '#Discover > div > div > div.d-flex.flex-column.col-lg-8.col-12.mt-lg-0.pr-0 > div.mt-lg-0.order-lg-3.order-md-3.order-sm-3.pr-xsm > div:nth-child(1) > div.d-flex.flex-row.align-items-center > div > h1')
                                    ))
    while not isinstance(driver.find_element_by_css_selector('#Discover > div > div > div.d-flex.flex-column.col-lg-8.col-12.mt-lg-0.pr-0 > div.mt-lg-0.order-lg-3.order-md-3.order-sm-3.pr-xsm > div:nth-child(1) > div.d-flex.flex-row.align-items-center > div > h1'
                                                            ).text,str):
        time.sleep(.1)
        job = driver.find_element_by_css_selector('#Discover > div > div > div.d-flex.flex-column.col-lg-8.col-12.mt-lg-0.pr-0 > div.mt-lg-0.order-lg-3.order-md-3.order-sm-3.pr-xsm > div:nth-child(1) > div.d-flex.flex-row.align-items-center > div > h1'
                                                ).text
        if job[:3] == 'Jobs':
            break
    search_bar = driver.find_element_by_id('sc.keyword')
    search_bar.send_keys(name)
    dropdown_type = driver.find_element_by_css_selector('#scBar > div > div.ml-xsm.search__SearchStyles__searchDropdown.css-j7qwjs.ew8s0qn0 > div > div.selectedLabel'
                                                        ).click()

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID,
                                                                    'option_1')
                                                                    ))
    driver.find_element_by_id('option_1').click()
    try:
        labels = driver.find_elements_by_class_name('selectedLabel')
        correct_text = [option for option in labels if (option.text == 'Unternehmen')]
        while not correct_text:
            time.sleep(.1)
            driver.find_element_by_id('option_1').click()
    except ElementNotInteractableException:
        pass
    if not isinstance(city, float):
        select_city = driver.find_element_by_id('sc.location')
        select_city.send_keys(Keys.CONTROL, 'a')
        select_city.send_keys(Keys.BACKSPACE)
        select_city.send_keys(city)
    driver.find_element_by_css_selector('#scBar > div > button').click()
    try:
        # could be detail or many page that comes up - only explicit wait is possible
        time.sleep(3)
        profiles = driver.find_elements_by_xpath("//*[@class='single-company-result module ']")
        if profiles:
            for i in range(0, (len(profiles)+2)):
                try:
                    profile = driver.find_element_by_css_selector(f'#MainCol > div > div:nth-child({i}) > div > div.col-lg-7 > div > div.col-9.pr-0 > h2 > a').text
                    recc = driver.find_element_by_css_selector(f'#MainCol > div > div:nth-child({i}) > div > div.col-lg-5.ei-contributions-count-wrap.mt-std > div > div.ei-contribution-wrap.col-4.pl-lg-0.pr-0 > a > span.num.eiHeaderLink').text
                    region = driver.find_element_by_css_selector(f'#MainCol > div > div:nth-child({i}) > div > div.col-lg-7 > div > div.col-9.pr-0 > div > p.hqInfo.adr.m-0 > span').text
                    profile_details.append([profile,recc,region])
                except NoSuchElementException:
                    pass
            # formatting thousands recommendation numbers:
            for candidate in profile_details:
                if 'Tsd' in candidate[1]:
                    num_candidate = candidate[1][:candidate[1].find(' ')]
                    if ',' in num_candidate:
                        float_candidate = float(num_candidate.replace(',','.'))
                    else:
                        float_candidate = float(num_candidate)
                    form_candidate = int(1000*float_candidate)
                    candidate[1] = form_candidate
                elif '-' in candidate[1]:
                    candidate[1] = 0
                else:
                    candidate[1] = int(candidate[1])
            # checking first result (usually the best):
            if (fuzz.partial_ratio(cleaning(
                profile_details[0][0]).lower(), cleaning(name).lower()) > 50
                ) & (profile_details[0][1] > 100):
                    choice = profile_details[0]
                # when first result is inconclusive, we check all results
            if not choice:
                choice = matching(profile_details,name)
            link = driver.find_element_by_link_text(choice[0]
                                                    ).get_attribute("href")
            title = choice[0]
            print(choice)
            driver.get(link)
        else:
            link = driver.current_url
            title = driver.find_element_by_css_selector('#EmpHeroAndEmpInfo > div.empInfo.tbl.hideHH > div.header.cell.info > h1'
                                                        ).text
            print(title)
        try:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
                                    (By.CLASS_NAME,'my-0.css-16jzkgq')
                                    ))
            while not isinstance(driver.find_element_by_class_name('my-0.css-16jzkgq'
                                                                    ).text,str):
                time.sleep(.1)
                subtitle = driver.find_element_by_class_name('my-0.css-16jzkgq'
                                                            ).text
                if subtitle[:8] == 'Überblick':
                    break
            recc_percent = driver.find_element_by_css_selector('#EmpStats_Recommend > div > div.d-lg-table-cell.posRl.css-1h8alnk.e1chagab0 > svg > text'
                                                            ).text  
            ceo_percent = driver.find_element_by_css_selector('#EmpStats_Approve > div > div.d-lg-table-cell.posRl.css-1h8alnk.e1chagab0 > svg > text'
                                                            ).text
            if driver.find_element_by_css_selector('#EIOverviewContainer > div > div.mt-std.pb-0.gd-ui-module.css-11b1byw.ec4dwm00 > div > div:nth-child(2) > div:nth-child(3) > button'
                                                    ):
                driver.find_element_by_css_selector('#EIOverviewContainer > div > div.mt-std.pb-0.gd-ui-module.css-11b1byw.ec4dwm00 > div > div:nth-child(2) > div:nth-child(3) > button'
                                                    ).click()
                time.sleep(1)
            interview_percents = driver.find_elements_by_class_name('ml-auto')
            interview_categories = driver.find_elements_by_class_name('mr-xsm')
            try:
                for q in range(0, 3):
                    row = driver.find_elements_by_class_name(f'd-flex.pb-sm.c{q}.css-3uwbvr.ee3ubnb1')
                    row_formatted = [element.text for element in row]
                    halves_a = row_formatted[0].split()
                    percent_exp_a = halves_a[1]
                    if halves_a[0].lower() == 'positiv':
                        int_positive = percent_exp_a
                    if halves_a[0].lower() == 'negativ':
                        int_negative = percent_exp_a
                    if halves_a[0].lower() == 'neutral':
                        int_neutral = percent_exp_a
                    halves_b = row_formatted[1].split()
                    percent_exp_b = halves_b[1]
                    if halves_b[0].lower() == 'online-bewerbung':
                        detail_parameter_a = percent_exp_b
                    if halves_b[0].lower() == 'personalvermittler':
                        detail_parameter_b = percent_exp_b
                    if halves_b[0].lower() == 'empfehlung':
                        detail_parameter_c = percent_exp_b
                    if halves_b[0].lower() == 'hochschul-recruiting':
                        detail_parameter_d = percent_exp_b
                    if halves_b[0].lower() == 'persönlich':
                        detail_parameter_e = percent_exp_b
                    if halves_b[0].lower() == 'sonstiges':
                        detail_parameter_f = percent_exp_b
                    if halves_b[0].lower() == 'vermittlungsagentur':
                        detail_parameter_g = percent_exp_b
                for l in range(3, 7):
                    row_detail_formatted = driver.find_element_by_class_name(f'd-flex.pb-sm.c{l}.css-dwl48b.css-1adfoly.ee3ubnb1'
                                                                            ).text
                    half = row_detail_formatted.split()
                    perc_int = half[1]
                    if half[0].lower() == 'online-bewerbung':
                        detail_parameter_a = perc_int
                    if half[0].lower() == 'personalvermittler':
                        detail_parameter_b = perc_int
                    if half[0].lower() == 'empfehlung':
                        detail_parameter_c = perc_int
                    if half[0].lower() == 'hochschul-recruiting':
                        detail_parameter_d = perc_int
                    if half[0].lower() == 'persönlich':
                        detail_parameter_e = perc_int
                    if half[0].lower() == 'sonstiges':
                        detail_parameter_f = perc_int
                    if half[0].lower() == 'vermittlungsagentur':
                        detail_parameter_g = perc_int
            except NoSuchElementException:
                pass
        except BaseException as error:
            print(error)
        finally:
            sed = (name, 
                    title,
                    city,
                    recc_percent,
                    ceo_percent,
                    int_positive,
                    int_negative,
                    int_neutral,
                    detail_parameter_a,
                    detail_parameter_b,
                    detail_parameter_c,
                    detail_parameter_d,
                    detail_parameter_e,
                    detail_parameter_f,
                    detail_parameter_g,
                    link,
                    'a'
                    )
            company_data.loc[len(company_data)] = sed
    except BaseException as e:
        sed = (name, city, 'b')
        no_results_df.loc[len(no_results_df)] = sed
        print(e)
# %%
# Explorative part:
listings = pd.DataFrame(columns=('company',
                                'job_title',
                                'location',
                                'available_since',
                                'job_description',
                                'link/request'
                                ))
for i, row in company_data[0:1].iterrows():
    name = row['Input_name']
    choice = row['glassdoor_name']
    city = row['city']
    link = row['link/request']
    driver.get(link)
    print("acquiring listings for: "+name+" ...")
    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
                                        (By.CLASS_NAME,'my-0.css-16jzkgq')
                                        ))
        while not isinstance(driver.find_element_by_class_name('my-0.css-16jzkgq'
                                                                ).text,str):
            time.sleep(.1)
            subtitle = driver.find_element_by_class_name('my-0.css-16jzkgq'
                                                        ).text
            if subtitle[:8] == 'Überblick':
                break
        jobs = driver.find_element_by_css_selector('#EIProductHeaders > div > a.eiCell.cell.jobs')
        jobs.click()
        WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
                                        (By.CSS_SELECTOR,'#JobsLandingPage > div > div > div > div.module.gdGrid > header > div > h1')
                                        ))
        while not isinstance(driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.module.gdGrid > header > div > h1'
                                                                ).text,str):
            time.sleep(.1)
            subtitle = driver.find_element_by_class_name('#JobsLandingPage > div > div > div > div.module.gdGrid > header > div > h1'
                                                        ).text
            if subtitle[:3] == 'Jobs':
                break
        listed = driver.find_elements_by_class_name('JobsListStyles__newJobListItem.pt-std')
        listings_count = len(listed)
        if listed:
            total_listings = driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.module.gdGrid > div.d-flex.align-items-center.my.justify-content-between > div:nth-child(1) > strong'
                                                ).text
            if int(total_listings) > 40:
                if int(total_listings) > 1000:
                    dropdown_range = driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.module.gdGrid > div.d-flex.align-items-center.my.justify-content-between > div.d-flex.align-items-center > div > button'
                                                                        ).click()
                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                    '#JobsLandingPage > div > div > div > div.module.gdGrid > div.d-flex.align-items-center.my.justify-content-between > div.d-flex.align-items-center > div > div > ul > li:nth-child(2) > a')
                                                                    ))
                    driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.module.gdGrid > div.d-flex.align-items-center.my.justify-content-between > div.d-flex.align-items-center > div > div > ul > li:nth-child(2) > a'
                                                        ).click()
                    time.sleep(.4)
                    max_amount = 1000
                # between 40 and 100 - iterate the pages as they are until 100:
                else:
                    max_amount = 100
            else:
                max_amount = 40
            listing_counter = 0
            while (listing_counter <= max_amount) and (listing_counter <= int(total_listings)):  
                page_entries = driver.find_elements_by_class_name('JobsListStyles__newJobListItem.pt-std')
                for m in range(1, len(page_entries)+1): 
                    try:
                        WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
                                                        (By.CSS_SELECTOR,'#JobsLandingPage > div > div > div > div.module.gdGrid > header > div > h1')
                                                        ))
                        listing = driver.find_element_by_css_selector(f'#JobsLandingPage > div > div > div > div.module.gdGrid > div.JobsListStyles__jobListContainer.gdGrid > ul > li:nth-child({m}) > div > div > div > div > div > a')
                        title = listing.text
                        link = listing.get_attribute('href')
                        duration = driver.find_element_by_css_selector(f'#JobsLandingPage > div > div > div > div.module.gdGrid > div.JobsListStyles__jobListContainer.gdGrid > ul > li:nth-child({m}) > div > div > div > div > div > div.JobDetailsStyles__companyInfo.pt-xxsm.d-flex.justify-content-between.JobDetailsStyles__newMobileFontSize > span.JobDetailsStyles__newGrey'
                                                                        ).text
                        location = driver.find_element_by_css_selector(f'#JobsLandingPage > div > div > div > div.module.gdGrid > div.JobsListStyles__jobListContainer.gdGrid > ul > li:nth-child({m}) > div > div > div > div > div > div.JobDetailsStyles__companyInfo.pt-xxsm.d-flex.justify-content-between.JobDetailsStyles__newMobileFontSize > span.JobDetailsStyles__newLocationGrey'
                                                                        ).text
                        description = 'desc'
                        driver.get(link)
                        try:
                            url = driver.current_url
                            url = url[url.find('de')+2:url.find('de')+14]
                            if url == '/job-listing':
                                # deatiled Job-listing
                                WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
                                        (By.CSS_SELECTOR,'#JobDescriptionContainer > div.css-t3xrds.ecgq1xb2')
                                        ))
                                driver.find_element_by_css_selector('#JobDescriptionContainer > div.css-t3xrds.ecgq1xb2'
                                                                    ).click()
                                time.sleep(.1)
                                description = driver.find_element_by_id('JobDescriptionContainer'
                                                                        ).text
                            else:
                                # market-joblisting
                                WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
                                        (By.CSS_SELECTOR,'#JobDescriptionContainer > div.css-t3xrds.e856ufb2')
                                        ))
                                driver.find_element_by_css_selector('#JobDescriptionContainer > div.css-t3xrds.e856ufb2'
                                                                    ).click()
                                time.sleep(.1)
                                description = driver.find_element_by_id('JobDescriptionContainer'
                                                                        ).text
                        except NoSuchElementException:
                            pass
                        finally:
                            driver.back()
                        sed = [name,
                                title,
                                location,
                                duration,
                                ' '.join(description.split()),
                                link]
                        listings.loc[len(listings)] = sed
                        listing_counter += 1
                        if (listing_counter >= max_amount) or (listing_counter >= int(total_listings)):
                            break
                    except NoSuchElementException:
                        pass
                driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.d-flex.flex-column.align-items-top.mt > div > div.pageContainer > button.nextButton.css-sed91k > span > svg'
                                                        ).click()
                time.sleep(3)
                try:
                    if (listing_counter >= max_amount) or (listing_counter >= int(total_listings)):
                        break
                    driver.find_element_by_css_selector('#JobsLandingPage > div > div > div > div.d-flex.flex-column.align-items-top.mt > div > div.pageContainer > button.nextButton.css-sed91k > span > svg'
                                                        ).click()
                    time.sleep(3)
                except NoSuchElementException:
                    break
        else:
            break
    except BaseException as error:
        print(error)
# %%
listings.to_excel('listings.xlsx',sheet_name='listings')
company_data.to_excel('results.xlsx',sheet_name='listings')  