#%%
from cmath import nan
import time
from time import sleep
from decouple import config
import pandas as pd
import requests
from fuzzywuzzy import process, fuzz
from collections import Counter
import math

from pynput.keyboard import Key, Controller
from pynput import keyboard

# Selenium Webdriver:
import datetime
from datetime import datetime
from selenium import webdriver                      

from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import TimeoutException
from selenium.common import exceptions 
from selenium.webdriver.common.keys import Keys                                    
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import Select 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#%%
path_chromedriver = r'C:\Users\jonathan.kammering\OneDrive - Statista GmbH\Desktop\Python\Scraping\system\chromedriver_win32\chromedriver.exe'
options = Options()
options.page_load_strategy = 'eager'
# options.add_argument('--headless')
options.add_argument("--window-size=1920,1080")
driver = webdriver.Chrome(path_chromedriver, options=options)
#%%
f_mail = 's.meischke91@gmail.com'
f_pass = 'Varianz23'
driver.get('https://www.glassdoor.de/member/home/index.htm')
time.sleep(2)
driver.find_element_by_id('onetrust-accept-btn-handler').click()
time.sleep(2)
email_field = driver.find_element_by_id('inlineUserEmail')
email_field.send_keys(f_mail)
pass_field = driver.find_element_by_id('inlineUserPassword')
pass_field.send_keys(f_pass)
driver.find_element_by_css_selector('#InlineLoginModule > div > div.mx-auto.my-0.maxw-sm-authInlineInner.px-std.px-sm-xxl.d-flex.flex-column.mw-400 > div > form > div.d-flex.align-items-center.flex-column > button > span'
                                    ).click()
# %%
filep = r'C:\Users\jonathan.kammering\OneDrive - Statista GmbH\Desktop\Python\Scraping\Scraping_intel\DATA\Kopie von datadummy_glassdoor.xlsx'
#create DataFrame for use in the iteration
df = pd.read_excel(filep, sheet_name='gehalt', usecols='A:B')

job_results_df = pd.DataFrame(columns=('position',
                                        'city',
                                        'trust_score',
                                        '#_quotes',
                                        'average_month',
                                        'average_yrl',
                                        'average_bonus',
                                        'bonus_range'
                                        ))
url_wages = 'https://www.glassdoor.de/Geh%C3%A4lter/index.htm'
driver.get(url_wages)
time.sleep(2)
for i, row in df.iterrows():
    trust = None
    av_yrl = None
    av_mnthl = None
    boni_range = None
    boni_av = None
    basis = None
    position = row['position']
    city = row['stadt']
    print('processing: '+str(position))
    if isinstance(city, float):
        print('no city input')
        continue
    time.sleep(.5)
    driver.find_element_by_id('KeywordSearch').clear()
    driver.find_element_by_id('KeywordSearch').send_keys(position)
    driver.find_element_by_id('LocationSearch').clear()
    driver.find_element_by_id('LocationSearch').send_keys(city)
    driver.find_element_by_id('HeroSearchButton').click()
    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                    '#nodeReplace > div > div > div:nth-child(1) > div > div.mt > div.row.mt-lg > div.col-12.col-lg-4 > aside > figcaption > span > p')
                                                                    ))
        try:
            trust = driver.find_element_by_css_selector('#nodeReplace > div > div > div:nth-child(1) > div > div.mt > div.row.mt-lg > div.col-12.col-lg-4 > aside > figcaption > span > p'
                                                    ).text
            basis = driver.find_element_by_css_selector('#nodeReplace > div > div > div:nth-child(1) > div > div.mt > div.row.mt-lg > div.col-12.col-lg-4 > p'
                                                    ).text
        except NoSuchElementException:
            print('a')              
            continue
        average_sal = driver.find_element_by_css_selector('#nodeReplace > div > div > div:nth-child(1) > div > div.mt > div.row.mt-lg > div.col-12.col-lg-4 > div.d-flex.align-items-baseline > span.m-0.css-146zilq.ebrouyy2'
                                                    ).text
        index = driver.find_element_by_class_name('m-0.css-18stkbk').text
        if driver.find_element_by_class_name('m-0.css-18stkbk'
                                            ).text.strip() == '/Jahr':
            av_yrl = average_sal
        else:
            av_mnthl = average_sal
        try:
            if driver.find_element_by_css_selector('#nodeReplace > div > div > div:nth-child(1) > div.row.mt > div > div.mt > div:nth-child(1) > p'
                                                ).text.lower()[:5] == 'für d':
                boni_av = None
                boni_range = None
        except NoSuchElementException:
            try:
                elements_average = driver.find_elements_by_class_name('m-0.css-93svrw.ebrouyy3')
                boni_av = elements_average[0].text
                elements_range = driver.find_elements_by_class_name('m-0.css-1b6bxoo')
                # boni_range = elements_range[2].text
                if elements_range[1].text == 'Durchschnittl.':
                    boni_range = elements_range[3].text
                elif elements_range[1].text == 'Spanne':
                    boni_range = elements_range[2].text
                else:
                    boni_range = elements_range[1].text
            except IndexError:
                continue
    except BaseException as error:
        print(error)
    finally:
        sed = (position,
                city,
                trust,
                basis,
                av_mnthl,
                av_yrl,
                boni_av,
                boni_range
                )
        job_results_df.loc[len(job_results_df)] = sed
        driver.back()
# %%