# Glassdoor Scraper

**_This scraper is aimed at making requests to the www.glassdoor.de website, identifying a correct profile on the page, as well as extracting relevant data therein._**

This Automation Script works through interacting with the Browser of choice, by use of a so-called “Driver”, which is not to be confused with the Selenium Webdriver. 
The Browser-Driver can be downloaded for free for all common browsers. It is vital to have a browser-version matching the driver version. Installation and further details can be found online (https://chromedriver.chromium.org/downloads). 
The Selenium WebDriver offers a testing framework for automation. In our case, we use it to interact with html elements, those of the Glassdoor webpage in particular.
Furthermore, it is vital to make use of a Virtual Private Network (VPN) in order to avoid being detected as a bot. Websites will routinely check for this and block IP addresses temporarily when automation is detected.

## Methods

* Web-Automation: Selenium Webdriver (3.141.0)

* Driver (Browser): ChromeDriver

* Data objects: Pandas DataFrames

## Errors

The ‘ConnectionRefusedError’ can be caused by neglecting to use a VPN. When a VPN is used and the error perseveres, it will mean that the Glassdoor servers are having a problem, or that your VPN might not function correctly. Changing the location of the VPN IP-address is sometimes helpful. In other cases, I have had to wait for a few hours, after which everything would work as expected.<br/>
In a third case, this message comes up, when the driver is not active or not configured correctly. Please check the Selenium Documentation for correct use: https://selenium-python.readthedocs.io/getting-started.html

## Files

The script scraper is divided into two separate functions. One focuses on the identification of the correct profiles, as well as acquiring the profile data therein (glassdoor_profiles). The second script is aimed at scraping data about specific job titles from the website (glassdoor_jobs.py).

## Browser-Driver

The Browser-Driver can easily be found on the internet. Again, please make sure that your browser version is stated to be corresponding with the version of your Browser-Driver.<br/>

Secondly, Browser-Driver will need to be unpacked and saved to the desktop, its location is used in the next step.

## Quick-Setup

1. The location of the browser-driver needs to be fit (path\_chromedriver) <b>line 26</b>
2. The login credentials need to be updated in <b>lines 33,34</b> (login can also be done by hand after initiating the driver window)
3. In <b>line 46</b>, the location of the input excel file needs to be redefined (filep)
  1. Make sure that the structure of the file corresponds to the DataFrame created in the script (<b>line 48</b>: here a sheet name and the columns are defined for loading in the data
  2. A one line header is default setting, otherwise check the documentation for different header preferences ([https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read\_excel.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html))
4. To convert the resulting DataFrames to Excel, the target location can be changed in <b>lines 467, 468</b> (df.to\_excel)

## Detailed Process

## 1. Profiles

Firstly, imports are being done. The second cell defines the path to the Chromedriver (path_chromedriver), as well as other options for the WebDriver. The headless option results in the browser window not showing on screen. 
For interaction with the website, the window must be maximized, this is also done in this cell. In the third cell the login credentials are handed over to the site and the login commences. 
Next, the input file is read into a DataFrame. This DataFrame is checked for its most common abbreviations, to help with the fuzzy match later in the identification process. 
The functions “cleaning” and “matching” are used in the fuzzy match process. The cell “Scrape profile data” contains the actual scraping process. First the target DataFrame is defined (company_data). 
It will hold the scraped data. A second DataFrame is created to hold searches that did not return results in the process (no_results_df).  A number is set to zero to count the iterations of the loop.

Next, the loop starts. “.iterrows()” iterates over rows in a tuple fashion (# of iterations, row). 
The specific row is a one dimensional Series. All parameters to be filled by the scraper are set to zero. 
Furthermore, the iteration is counted at this point through the variable “number”. 
Next, the information from the input DataFrame is converted into variables to be used when searching for data.
For other scrapers it might be useful to make requests through the URLs, with Glassdoor this is unfortunately not possible. 
In most cases the “WebDriverWait” method, in conjunction with a CSS element, is used in this script. Other waiting processes are based on a loop that waits for a specified amount of time before carrying on with the analysis. 
One such example can be found in <b>line 163</b>. Here, a certain object is read. Based on whether the object is a string or a float object, the state of the DOM is acquired and waiting happens, if when needed. 
When the page is sufficiently loaded, the search bar is fed the information of the DataFrame. Therein, the dropdown (div.selectedLabel) is being clicked, and (after the target element becomes visible in the DOM) the correct element is selected. 
The following “try except” block covers the wait for the dropdown’s content.<br/>

Next, the information on the HQ location of the company is handed over to the site. 
First, the input information is checked. An empty city input is translated to a NaN value, which is per definition a float value in Python. 
Thus, this is checked before sending the information to the search bar. After handing over the information, the “search” button is clicked in <b>line 192</b>. 
The wait in this case is explicit because there are two different pages that might come up after clicking. This entails different elements that could be waited for, thus, the process is simply paused for three seconds. 
Following the waiting process, the variable “profiles” is filled with the search results. Its length indicates the number of results. 
If any profiles were found, these profiles are then iterated one by one. Here, the order of the elements starts at zero, but always skips exactly two elements in the process.
To mitigate this problem, the loop runs over the number of profiles plus two. For any result there should be: The profile name (profile), the recommendation score (recc) and the location of the HQs (region).
If any of these are not present, the site has a problem and this error is caught in NoSuchElementException and passes the loop at this point for the specific profile. Next, the acquired data is appended to the list profile_details. 
This list holds the three data points on each search result. 

Between <b>lines 207 and 219</b>, the recommendation score of each profile is converted to an integer from the format: “[number] Tsd.”. This happens by finding the “Tsd” keyword and then isolating it. 
Furthermore, the german comma for thousands digits is replaced by the English dot. <br/>
Followingly, a check of the first result is conducted. This has shown to be efficient, because the Glassdoor algorithm is usually correct in its relevancy decision. 
Therefore, the fuzzywuzzy package is used to check whether the string of the first result contains the string that is needed. 
If this partial ratio (fuzz.partial_ratio) is larger than 50 and the recommendations are over 100, the rest of the search results are disregarded. 
This has shown to be a big time-saver, while the Glassdoor algorithm has shown to be very precise on top of that. 
If the first item was not pre-selected, the list matching function “matching” is used to match all search results against the target value. Choice represents the chosen profile. Its hyperlink is retrieved. As well as the name. 
In <b>line 233</b>, the else statement catches the case, where the Glassdoor algorithm is sufficiently sure about its analysis to direct the user to a profile detail page immediately. Here the list profiles will be empty. 
In this case the needed information is read out on the spot. <br/>

Furthermore, the process waits for the profile page to load. The recommendation in percents are read, as well as the recommendation score for the CEO. 
<b>Lines 253 to 257</b> open a window that holds the information on the different interview categories, as well as their connected percentages. 
These would otherwise not be visible. An explicit wait is needed here, because it is unknown which elements exactly are going to be included. Depending on the actual given recommendations some might be missing.<br/>
Interview_percents and interview_categories are lists that hold all the information in the window. The following “try-except” block deals with checking each option that might be available and reading it into a variable. <br/>
The following try and except block extracts the information from the drop down menu. Here, the elements are being read into a list first. 
Then, the list is dissected piece by piece. First, the .text method is used to convert all elements by way of a list comprehension. 
The if-statements are indicative for which element is at hand. If one is found, it is assigned to the corresponding value in the results list (sed). Furthermore, the retrieved data is separated into two sets (halves_a, halves_b). 
These separate the two different pie charts information (dropdown).

## 1. Listings

The scraping process retrieving the job listings follows the same structure.
The target DataFrame is defined in the beginning. Then, the company_data is iterated. Firstly, the profile page is loaded.
The next try except block encapsulates the listings retrieval. Here, the process waits for the string “Überblick […]” to be visible. 
A while loop checks that this string is visible before the jobs button is found and clicked. The next wait tries to find a css selector: “Jobs bei […]”. This is also checked by the same while loop structure as used before. 
The variable “listed” holds all job elements on the page. In the next if else intersection, this list is tested for content. 
If content is found in the list, the total number is retrieved, upon which a decision is made on the amount to be retrieved. 
Here, “max_amount” is set to 40 if there are fewer than 40 entries. If there are more than 1000 entries, the maximum is set to 1000 and only the last 7 days are regarded (.click()) on dropdown element). 
Otherwise (more than 40 less than 1000), the maximum is set to 100. A counter for the listings is created (listings_counter). The next section deals with the iteration through the result pages. 
A while loop ensures that two conditions are met: 1) the listings_counter cannot be larger than the max_amount set and 2) the listings_counter must be smaller or equal to the total_listings. 
The page entries are retrieved and handed over to a variable (page_entries). Then, a for loop encapsulates the iteration over these elements. The page always starts at one and ends one entry further than expected. 
Therein, the title, link and duration (how long has the listing been available) are retrieved. Once this information has been retrieved, the process opens the connected page of the listing. 

Through the URL, the process checks for the two different types of pages that could be behind the link: market or detailed job listing. These hold essentially the same information, but the look different, which is why different elements are targeted for each.<br/>
Here, the job description is retrieved. In the “except block”, the NoSuchElementException catches any elements that are not available on the page and skips over this element in the process. 
The “finally” statement is followed by the command to skip back to the listings page. Lastly, the information is read into the list “sed”, which is set as the newest row available in the target DataFrame (listings). 
When the page has been iterated completely, the conditions are checked again, the “except block” here catches any listings that are unavailable on the listings page and passes over them. 
Finally, the process checks the conditions again and then clicks on the “next-button” on the page. In case there are no listings, the else statement at the end breaks the specific loop.







